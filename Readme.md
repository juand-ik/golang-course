# Go course

## Documentation Go

This document is for reference

### Variables

Examples:

```
var suma int = 8 + 3;
var nombre string = "Juand";
var condicion boolean = true;
```

#### Variables inferred

```
var string_inferida := "texto";
var int_inferida := 13;
```

#### Variables custom - Structs


Example:

```
type car struct{
	name string
	color string
	precio float32
}
```

### Functions

Example:

#### Simple

```
func simple(){
	...
}
```
#### With params

```
func params(n1 int, n2 int, n3 string){
	...
}
func paramsWithReturn(n1 int, n2 int) float32{
	...
}
```

In one parameter:

```
func oneP(elements ...string){
	for _, elements := range elements{
		fmt.Println(elements);
	}
}

```

### Arrays

Example:

```
var movies [3]string;
movies[0] = "Scary movie";
movies[1] = "Gumanji";
movies[2] = "Back to the future";

fmt.Println(movies[2]);
```

```
movies := [3] string{"batman", "insepction", "matrix"};
fmt.Println(movies[2]);
```

*Slice*
```
var movies := []string{
	"Batman",
	"Shazam",
	"Dora la exploradora"};

movies = append(movies, "Jobs");
fmt.Println(movies);
fmt.Println(movies[0:3]);
```

* Note: You can see the length using:
```
fmt.Println(len(movies));
```

#### MultiArray

```
var movies [3][2]string;
movies[0][0] = "Scary movie";
movies[0][1] = "Gumanji";

movies[1][0] = "Back to the future";
movies[1][1] = "Back to the future II";

movies[2][0] = "Ironman";
movies[2][1] = "Shazam";

fmt.Println(movies[2][1]);
```

### Conditionals and bucles

#### If

Example:

```
package main

import "fmt"

func main(){
	age := 18;

	if age >= 18{
		fmt.Println("Kid")
	}else if age <= 65{
		fmt.Println("Adult")
	}else{
		fmt.Println("Old")
	}
}
```

* Note: Passing parameters by terminal(need use `os` library).

```
package main

import("fmt", "os", "strconv")

func main(){
	fmt.Println("hi! " + os.Args[1]);
	age, err := strconv.Atoi(os.Args[2]);
	fmt.Println(err);
	fmt.Println(age);
}
```
In terminal:
```
go main.go Juan 27
```
* Note if you no use a variable, you can use `_`, example:

```

...

age, _ := strconv.Atoi(os.Args[2]);
fmt.Println(age);
...

```
#### For

Example:

```
for i := 0; i < 10; i++{
	fmt.Println("number:", i)
}
```

```
movies := []string{"Matrix", "IT", "Ready player one"}

for i := 0; i < len(movies); i++{
	fmt.Println("Movie: " + movie[i] + " number:", i);
}
```

```
for _, movie := range movies{
	fmt.Println(movie);
}
```

#### Switch

Example:

```
...

moment := time.Now();
today  := moment.Weekday();

switch today{
	case 0:
		fmt.Println("Sun")
	case 1:
		fmt.Println("Mon")
	case 2:
		fmt.Println("Tue")
	case 3:
		fmt.Println("Wen")
	default:
		fmt.Println("Other day")
}

...

```

### Files

#### Read from file

Example:

```
package main

import "fmt"
import "io/ioutil"

func main(){
	fmt.Println("Read");

	text_file, errorFile := ioutil.ReadFile("file.txt");
	showError(errorFile);

	fmt.Println(string(text_file));

}
func showError(e error){
	if(e != nil){
		panic(e)
	}
}

```

#### Write in file

```
package main

import "fmt"
import "io/ioutil"
import "os"

func main(){
	fmt.Println("Write in file");

	/*input_text := []byte(os.Args[1]);
	to_write   := ioutil.WriteFile("file.txt", input_text, 0777);
	showError(to_write);*/

	// Or
	new_text := os.Args[1] + "\n";

	file, err := os.OpenFile("file.txt", os.O_APPEND, 0777)
	showError(err);

	write, err := file.WriteString(new_text);
	fmt.Println(write);
	showError(err);

	file.Close();


	text_file, errorFile := ioutil.ReadFile("file.txt");
	showError(errorFile);

	fmt.Println(string(text_file));

}
func showError(e error){
	if(e != nil){
		panic(e)
	}
}

```



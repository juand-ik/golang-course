# API

## Description

API - Go Example

## Tech briefing

* Go go1.13 linux/amd64
* Mongo

### Plugins

* [Gorilla/mux](https://github.com/gorilla/mux).
* [mgo](https://labix.org/mgo).
* [bson](https://gopkg.in/mgo.v2/bson).
